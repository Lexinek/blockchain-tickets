pragma solidity ^0.4.11;


contract Ticket {
  struct owner { string publicKey; uint price; }

  uint public listedPrice = 0;
  string public ticketHash;
  bool public listed = 0;
  owner[] private owners;
  uint private minPrice;
  uint private maxPrice;

  function Ticket(uint minPriceLimit, uint maxPriceLimit, string venueTicketHash, owner firstBuyer) {
    minPrice = minPriceLimit;
    maxPrice = maxPriceLimit;
    ticketHash = venueTicketHash;
    owners[msg.sender] = firstBuyer;
  }

  function getOwner() constant returns (owner) {
    //
  }

  function listForSale(string publicKey, uint priceToSellFor) {
    // owner verification + logic

    listed = 1;
    listedPrice = priceToSellFor;
  }

  function buy(string publicKey) {
    // listed checks + logic

    owners[msg.sender] = owner(publicKey, listedPrice);
    listed = 0;
  }

  function transfer(string publicKey) {
    // owner checks

    owners[/* dunno */] = owner(publicKey, 0);
  }
}


